//
//  CarInfoTableViewCell.swift
//  CarFaxAssignment
//
//  Created by Patel, Virenkumar on 8/16/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import UIKit

class CarInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var carPriceMileLocation: UILabel!
    @IBOutlet weak var carTitle: UILabel!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    let roundRadius = CGFloat(10.0)
    let buttonRoundRadius = CGFloat(5.0)
    
    weak var carSearchControllerDelegate: CarInfoCellCallback?
    var carIndexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        leftButton.useBlueBorder()
        leftButton.addButtonShadow()
        rightButton.useBlueBorder()
        rightButton.addButtonShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // IBActions
    @IBAction func callDealer(_ sender: Any) {
        if let indexPath = self.carIndexPath {
            carSearchControllerDelegate?.callDealer(indexPath: indexPath)
        }
    }
    @IBAction func viewDetails(_ sender: Any) {
    }

}
