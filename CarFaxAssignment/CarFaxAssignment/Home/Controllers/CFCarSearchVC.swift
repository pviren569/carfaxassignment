//
//  FirstViewController.swift
//  CarFaxAssignment
//
//  Created by Patel, Virenkumar on 8/16/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import UIKit
import SDWebImage

protocol CarInfoCellCallback: class {
    func callDealer(indexPath: IndexPath)
    func viewCarDetails(indexPath: IndexPath, carDetail: Int?)
}

class CFCarSearchVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    // Typealiases
    typealias completionHandler = (Bool) -> Void
    
    // Variables
    var carSearchResultViewModel: CarSearchResultsViewModel? {
        didSet {
            showCarSearchResults()
        }
    }
    var fetchDAO: CarSearchResultsProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLocalView()
        
        self.title = "Acura ILX"
        // Do any additional setup after loading the view.
    }
    
    func showCarSearchResults() {
        self.tableView.reloadData()
    }
    
    func setLocalView() {
        
        // For Testing purpose passing a fetchDAO. This will help ViewController to know where to fetch the data from(API or MocKdata)
        // while running the app fetchDAO will be empty so CarSearchDAO will fetch the data from API
        // While doing the Testing while creating the objec to CFCarSearchVC make fetchDAO = CarSeachMockDAO() so it will fetch the data from Mock.
        self.getCarSeachResult(dataAccessObject: fetchDAO ?? CarSearchDAO()) { (result) in
            if !result {
                // Display Error
            }
        }
    }
    
    func getCarSeachResult(dataAccessObject: CarSearchResultsProtocol,
                           onCompletion: @escaping completionHandler) {
        
        dataAccessObject.getCarSearchResults { (searchResponse) in
            if let searchResponse = searchResponse {
                self.carSearchResultViewModel = CarSearchResultsViewModel(carSearchResponse: searchResponse)
                onCompletion(true)
            } else {
                onCompletion(false)
            }
        }
    }

}

extension CFCarSearchVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carSearchResultViewModel?.numberOfRows ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellReuseID = "carInfoCell"
        
        guard let carsForDispaly = self.carSearchResultViewModel?.cars else {
            // TODO:: Need to Create a default cell This Error
            // This case should not happen.
            return tableView.dequeueReusableCell(withIdentifier: "", for: indexPath)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseID, for: indexPath)
        
        if let carInfoCell = cell as? CarInfoTableViewCell{
            let car = carsForDispaly[indexPath.row]
            carInfoCell.carImage.sd_setImage(with: car.mediumImageLink(), placeholderImage: UIImage(named: "icon_image_coming_soon"), options: [], context: nil)
            carInfoCell.carTitle.text = car.carTitleToDisplay()
            carInfoCell.carPriceMileLocation.text = car.priceMileLocationToDisplay()
            if let dealerPhone =  car.dealer?.phone, !dealerPhone.isEmpty {
                carInfoCell.leftButton.setTitle("\(dealerPhone.formatPhoneNumberForUI())", for: .normal)
            } else {
                carInfoCell.leftButton.isHidden = true
            }
            
            carInfoCell.carIndexPath = indexPath
            carInfoCell.carSearchControllerDelegate = self
        }
        return cell
    }
}

extension CFCarSearchVC: CarInfoCellCallback {
    func callDealer(indexPath: IndexPath) {
        if let delaerPhone = self.carSearchResultViewModel?.cars?[indexPath.row].dealer?.phone,  let phoneCallURL = URL(string:"tel://\(delaerPhone)") {
            if UIApplication.shared.canOpenURL(phoneCallURL) {
                UIApplication.shared.open(phoneCallURL)
            }
        }
    }
    
    func viewCarDetails(indexPath: IndexPath, carDetail: Int?) {
        // TODO Details Description of Car
    }
}

