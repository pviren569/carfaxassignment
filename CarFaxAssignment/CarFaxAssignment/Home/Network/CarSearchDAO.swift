//
//  CarSearchDAO.swift
//  CarFaxAssignment
//
//  Created by Patel, Virenkumar on 8/16/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import Foundation
typealias carSearchResultResponse = (CarSearchResult?) -> Void

protocol CarSearchResultsProtocol {
    func getCarSearchResults(completion: @escaping carSearchResultResponse)
}

struct CarSearchDAO : CarSearchResultsProtocol {
    func getCarSearchResults(completion: @escaping carSearchResultResponse) {
        
        let carSearchUrl = CredentialsHelper.sharedInstance().carSearchUrl()
        let reqConfig = RequestConfig(type: .Data, method: .Get, apiEndpoint: carSearchUrl, params: nil, httpBody: nil, headers: [:], background: true)
        
        CFRequest(reqConfig) { (data, response, error) in
            if let error = error {
                print("CarSearch: ERROR  Unable to complete request on endpoint: \(carSearchUrl) \nError: \(error)")
                completion(nil)
            }
            
            guard let carSearchData = data else {
                completion(nil)
                return
            }
            let jsonDecoder = JSONDecoder()
            
            do {
                let autoSearchResult = try jsonDecoder.decode(CarSearchResult.self, from: carSearchData)
                completion(autoSearchResult)
            } catch  {
                print("Unable to decode AutoSearchResult Response. Error is \(error)")
                completion(nil)
            }
        }
    }
}

