//
//  ViewModel.swift
//  CarFaxAssignment
//
//  Created by Patel, Virenkumar on 8/16/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import Foundation

struct CarSearchResultsViewModel  {
    
    var carSearchResponse : CarSearchResult
    
    var numberOfRows: Int? {
        return carSearchResponse.carList?.count
    }
    
    var cars: [Car]? {
        return self.carSearchResponse.carList
    }
}
