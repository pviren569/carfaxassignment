//
//  CredentialsHelper.swift
//  CarFaxAssignment
//
//  Created by Patel, Virenkumar on 8/16/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import Foundation


class CredentialsHelper: NSObject {
    class func sharedInstance() -> CredentialsHelper {
        
        struct Instance {
            static let credentialsHelper = CredentialsHelper()
        }
        return Instance.credentialsHelper
    }
    
    func baseUrl() -> String {
        return "https://carfax-for-consumers.firebaseio.com"
    }
    
    func carSearchUrl() -> String {
        return baseUrl() + "/assignment.json"
    }
    
    // For Future Use to pass default Parameters to request
    func cfDefaultParams() -> [String:String] {
        
//        var params : [String:String] = [:]
//        return params
        return [:]
    }
}
