//
//  Extensions.swift
//  CarFaxAssignment
//
//  Created by Patel, Virenkumar on 8/16/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func useBlueBorder() {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.blue.cgColor
    }
    
    func addButtonShadow() {
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1.25, height: 1.25)
        self.layer.shadowRadius = 0.75
        self.layer.shadowOpacity = 0.5
        self.clipsToBounds = false
        self.layer.masksToBounds = false
    }
}

extension String {
    func formatPhoneNumberForUI() -> String {
        
        if self.count == 10 {
            let formattedPhoneNumber: NSMutableString = NSMutableString(string: self)
            
            formattedPhoneNumber.insert("(", at: 0)
            formattedPhoneNumber.insert(")", at: 4)
            formattedPhoneNumber.insert(" ", at: 5)
            formattedPhoneNumber.insert("-", at: 9)
            
            return formattedPhoneNumber as String
        } else {
            return self
        }
    }
}
