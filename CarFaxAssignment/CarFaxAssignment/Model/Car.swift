//
//  Car.swift
//  CarFaxAssignment
//
//  Created by Patel, Virenkumar on 8/16/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import Foundation

struct CarSearchResult : Codable {
    var relatedCarLinks : RelatedCarInfo?
    var carList : [Car]?
    
    enum CodingKeys: String, CodingKey {
        case carList = "listings"
        case relatedCarLinks = "relatedLinks"
    }
}

struct Car: Codable {
    let price: Int?
    let dealer: Dealer?
    let mileage: Int?
    let make: String
    let model: String
    let year: Int
    let transmission: String?
    let carImage: CarImage?
    
    enum CodingKeys: String, CodingKey {
        case price = "currentPrice"
        case carImage = "images"
        case dealer, mileage, make, model, year, transmission
    }
    
    func mediumImageLink() -> URL? {
        let mediumImageLinkStr = self.carImage?.baseUrl?.appending("/1/344x258") ?? ""
        return URL(string: mediumImageLinkStr)
    }
    
    func largeImageLink() -> URL? {
        let largerImageLinkStr = self.carImage?.baseUrl?.appending("/1/640x480") ?? ""
        return URL(string: largerImageLinkStr)
    }
    
    func smallImageLink() -> URL? {
        let smallImageLinkStr = self.carImage?.baseUrl?.appending("/1/120x90") ?? ""
        return URL(string: smallImageLinkStr)
    }
    
    func carTitleToDisplay() -> String {
        return "\(year) \(make) \(model)"
    }
    
    func priceMileLocationToDisplay() -> String {
        var priceMileLocationToDisplay = ""
        if let price = currencyToDisplay() {
            priceMileLocationToDisplay = "\(price.replacingOccurrences(of: ".", with: ",")) | "
        }
        if let miles = milesToDisplay() {
            priceMileLocationToDisplay.append(contentsOf: "\(miles) | ")
        }
        if let address = dealerLocationToDisplay() {
            priceMileLocationToDisplay.append(contentsOf: "\(address)")
        }
        return priceMileLocationToDisplay
    }
    
    func milesToDisplay() -> String? {
        if let miles = self.mileage {
            if miles / 1000 > 0 {
                return "\(miles / 1000)k Mi"
            } else {
                return "\(miles)"
            }
        }
        return nil
    }
    
    func currencyToDisplay() -> String? {
        if let price = price as NSNumber? {
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.locale = Locale(identifier: "es_CL")
            return formatter.string(from: price)
        }
        
        return nil
    }
    
    func dealerLocationToDisplay() -> String? {
        if let city = dealer?.city, let state = dealer?.state {
            return "\(city), \(state)"
        }
        return nil
    }
}

struct CarImage: Codable {
    let baseUrl: String?
}

struct Dealer: Codable {
    let name: String?
    let address: String?
    let city: String?
    let state: String?
    let phone: String?
}

struct RelatedCarInfo: Codable {
    let similarCars: [SimilarCar]?
}

struct SimilarCar: Codable {
    let title: String?
    let externalLink: String?
    
    enum CodingKeys: String, CodingKey {
        case title = "text"
        case externalLink = "url"
    }
}
