//
//  CarSearchReultVCTests.swift
//  CarSearchReultVCTests
//
//  Created by Patel, Virenkumar on 8/16/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import XCTest
import FBSnapshotTestCase
@testable import CarFaxAssignment

class CarSearchReultVCTests: FBSnapshotTestCase {

    var viewController: CFCarSearchVC?
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        viewController?.fetchDAO = carSearchMockDAO()
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        viewController = storyboard.instantiateViewController(withIdentifier: "cfCarSearchVC") as? CFCarSearchVC
        // Snapshot Specific
        recordMode = false
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testCarSearchResult() {
        let mockDAO = carSearchMockDAO()
        if let viewController = viewController {
            let _ = viewController.view
            self.viewController?.getCarSeachResult(dataAccessObject: mockDAO, onCompletion: {_ in
                self.FBSnapshotVerifyView(viewController.view )
            })
        }
    }
    

}

class CarSearchXCTest: XCTestCase {
    
    var viewController: CFCarSearchVC?
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        viewController = CFCarSearchVC()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testCarModel() {
        carSearchMockDAO().getCarSearchResults { (response) in
            if let carSearchResullt = response {
                XCTAssertEqual("2014 Acura ILX", carSearchResullt.carList?[0].carTitleToDisplay())
                XCTAssertEqual("$11,994 | 82k Mi | Buena Park, CA", carSearchResullt.carList?[0].priceMileLocationToDisplay())
            } else {
                XCTFail()
            }
        }
    }
    
}
