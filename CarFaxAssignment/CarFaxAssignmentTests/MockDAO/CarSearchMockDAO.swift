//
//  CarSearchMockDAO.swift
//  CarFaxAssignmentTests
//
//  Created by Patel, Virenkumar on 8/16/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import Foundation
@testable import CarFaxAssignment

class carSearchMockDAO: CarSearchResultsProtocol {
    func getCarSearchResults(completion: @escaping carSearchResultResponse) {
        guard  let carSearchResultData = Utility().getMockData(forResource: "carSearchMock") else {
            completion(nil)
            return
        }
        
        do {
            let jsonDecoder = JSONDecoder()
            
            //decode mock into data structure
            let carSearchResult = try jsonDecoder.decode(CarSearchResult.self, from: carSearchResultData)
            completion(carSearchResult)
            
        } catch  {
            completion(nil)
        }
    }
}
