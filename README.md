
## CarFaxAssignment


## Getting Started || Clone a repository
1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

## Prerequisites
1. Xcode

## Installing
1. After cloning the repository, open CarFaxAssignment.xcworkspace in Xcode
2. Select iPhone Device(iPhone 7 Plus Simulator Preffered for Unit Case purposes)
3. Click Run(cmd + R). Menu -> Product -> Run

## Running the tests
1. If app isn't running on iPhone 7 plus Simulator 
    a. Go to CarFaxAssignmentTests.swift, change record mode(in setUp() method) value to true. 
    b. Run test for CarSearchReultVCTests class(Test case should fail at this time because FBSnapshot takes a screenshot at first time for reference)
    c.  Goto CarFaxAssignmentTests.swift, change record mode value to false. 
    d. Run test for CarSearchReultVCTests class(Test case should pass at this time because FBSnapshot compare the new image with reference image)
2. If app is running on iPhone 7 plus Simulator
    a. Goto CarFaxAssignmentTests.swift and run the test cases. 

## Built With
SDWebImage - This library provides an async image downloader with cache support.
iOSSnapshotTestCase - For Snapshot Testing

